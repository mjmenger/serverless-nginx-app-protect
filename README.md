# Serverless NGINX App Protect (SNAP)
This repo contains assets to deploy serverless NGINX App Protect WAF in AWS cloud. 
## Architecture
This repo pipline provisions SNAP in three stages. First uses terraform to create and manage infrastructure shown below. Next builds SNAP image and pushes it to AWS ECR. The last creates a service and a task to run SNAP instances in the ECS cluster created during first stage. 
![alt text](img/architecture.png "Architecture")
## How to Deploy
1. Get NGINX App Protect repo access certificate and key from F5 Networks ([link](https://www.nginx.com/free-trial-request))
2. Create AWS account with administrator permissions and generate access key ID and secret access key for the account
3. Create S3 bucket with default settings
4. Add CloudWatchFullAccess policy to ecsTaskExecutionRole role
5. Create a new gitlab repo for your SNAP deployment
6. Add following variables to the repo
    * AWS_ACCESS_KEY_ID (Get from AWS account)
    * AWS_SECRET_ACCESS_KEY (Get from AWS sccount)
    * AWS_DEFAULT_REGION (AWS deployment region. e.g.: us-east-2)
    * BUCKET (AWS S3 bucket to store terraform state)
    * NGINX_REPO_CRT (Convert your App Protect repo access cert to base64 form. e.g.: base64 -w0 nginx-repo.crt)
    * NGINX_REPO_KEY (Convert your App Protect repo access cert to base64 form. e.g.: base64 -w0 nginx-repo.key)
7. Clone this repo
8. Replace remote origin with your new gitlab repo
```
git remote rm origin
git remote add origin YOUR_NEW_GITLAB_REPO_URL
```
9. Set your newly created bucket in terraform/terraform.tf file
10. Push files to your new repo
As soon as CI/CD pipline ends successfully your serverless WAF is ready to use. Copy URL of newly created snap-alb AWS load balancer to browser to access your SNAP deployment.
## Roadmap
1. Serverless NGINX App Protect in AWS (Supported)
2. Serverless NGINX App Protect in GCP (In development)
3. Serverless NGINX App Protect in AWS (Future)
